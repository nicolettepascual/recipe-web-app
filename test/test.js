const chai = require('chai');
const expect = chai.expect;
const app = require('../app');
const http = require('chai-http');
chai.use(http);
const sinon = require('sinon');
const User = require('../api/model/user.model');
const Recipe = require('../api/model/recipe.model');

var clock;

describe('Basic App Tests', function () {
  this.slow(10000);
  before(function (done) {
    this.enableTimeouts(false)
    User.find({ 'email': 'test@user.com' }).deleteOne().then(res => {
      console.log('Test User removed');
      done();
    }).catch(err => {
      done(err);
      console.log(err.message);
    });
  });

  it('should exist', () => {
    expect(app).to.be.a('function');
  })

  it('should return 200 and message and get HTTP GET request in /', (done) => {
    chai.request(app).get('/')
      .then((res) => {
        expect(res).to.have.status(200);
        done();
      }).catch(err => {
        done(err);
        console.log(err.message);
      })
  });
});

describe('User Tests', function () {
  this.slow(10000);
  describe('Registration', () => {
    it('should return 200 and confirm valid input', (done) => {
      const new_user = {
        "name": "Test User",
        "email": "test@user.com",
        "password": "password"
      }
      chai.request(app).post('/register')
        .send(new_user)
        .then((res) => {
          expect(res).to.have.status(200);
          done();
        }).catch(err => {
          done(err);
          console.log(err.message);
        })
    });
  });

  describe('Login', function () {
    this.slow(10000);
    it('should return 200 and token for valid credentials', (done) => {
      const valid_input = {
        "email": "test@user.com",
        "password": "password"
      }
      chai.request(app).post('/login').send(valid_input).then((res) => {
        expect(res).to.have.status(200);
        done();
      }).catch(err => {
        done(err);
        console.log(err.message);
      })
    });
  });

  describe('Home Page Route', () => {
    it('should return 200 and user details if valid token is provided', (done) => {
      //mock login to get token
      const valid_input = {
        "email": "test@user.com",
        "password": "password"
      }

      chai.request(app).post('/login').send(valid_input).then((login_response) => {
        const token = 'Bearer ' + login_response.body.token;
        User.findOne({ 'email': valid_input.email }).then((found_user, err) => {
          chai.request(app).get('/?user=' + found_user._id).set('Authorization', token).then(protected_response => {
            expect(protected_response).to.have.status(200);
          }).catch(err => {
            console.log(err.message);
          });
        });
        done();
      }).catch(err => {
        done(err);
        console.log(err.message);
      });
    });
  });

  describe('Reset Password', () => {
    it('should return 200 and reset user password', (done) => {
      chai.request(app).put('/resetpassword').send({
        email: 'test@user.com',
        password: 'new password'
      }).then((res) => {
        expect(res).to.have.status(200);
        done();
      }).catch(err => {
        done(err);
        console.log(err.message);
      });
    });
  });

  describe('Refresh Auth Token', () => {
    before((done) => {
      clock = sinon.useFakeTimers();
      done();
    });

    after((done) => {
      clock.restore();
      done();
    });

    it('should still log in and refreshes token', (done) => {
      const valid_input = {
        "email": "test@user.com",
        "password": "new password"
      }
      clock.tick(3600100);
      chai.request(app).post('/login')
        .send(valid_input)
        .then((res) => {
          expect(res).to.have.status(200);
          done();
        }).catch(err => {
          done(err);
          console.log(err.message);
        })
    });
  });
});

describe('Recipe Tests', function () {
  this.slow(10000);
  /*before(function (done) {
    this.enableTimeouts(false)
    Recipe.find({ 'mealName': 'Meal Name' }).deleteOne().then(res => {
      console.log('Test Recipe removed');
      done();
    }).catch(err => {
      done(err);
      console.log(err.message);
    });
  });*/

  describe('Creating A Recipe', function () {
    it('should return 200 and have valid input', async () => {
      var user = await User.findOne({ 'email': 'test@user.com' });

      const new_recipe = {
        'mealName': 'Meal Name',
        'description': 'Meal Description',
        'ingredients': 'Meal Ingredients',
        'instructions': 'Meal Instructions',
        'prepTime': '30 mins.',
        'id': user._id
      };

      await chai.request(app).post('/add/recipe').send(new_recipe).then((res) => {
        expect(res).to.have.status(200);
      }).catch(err => {
        console.log(err.message);
      });
    });
  });

  describe('Retrieving A Recipe', function () {
    it('should return 200 and retreive specified recipe', async () => {
      var user = await User.findOne({ 'email': 'test@user.com' });

      await Recipe.findOne({ 'userId': user._id }, async function (err, posts) {
        recipeId = posts._id;
        await chai.request(app).get('/recipe/' + recipeId + '?user=' + user._id).then((res) => {
          expect(res).to.have.status(200);
        }).catch(err => {
          console.log(err.message);
        });
      });
    });
  });

  describe('Updating A Recipe', function () {
    it('should return 200 and update specified recipe', async () => {
      var user = await User.findOne({ 'email': 'test@user.com' });

      await Recipe.findOne({ 'userId': user._id }, async function (err, posts) {
        recipeId = posts._id;
        await chai.request(app).put('/edit/' + recipeId + '/' + user._id).send({
          mealName: 'New Meal Name',
          description: 'New Meal Description',
          ingredients: 'New Meal Ingredients',
          instructions: 'New Meal Instructions',
          prepTime: '40 mins.',
        }).then((res) => {
          expect(res).to.have.status(200);
        }).catch(err => {
          console.log(err.message);
        });
      });
    });
  });

  describe('Deleting A Recipe', function () {
    it('should return 200', async () => {
      var user = await User.findOne({ 'email': 'test@user.com' });

      await Recipe.findOne({ 'userId': user._id }, async function (err, posts) {
        recipeId = posts._id;
        await chai.request(app).get('/delete/' + recipeId + '/' + user._id).then((res) => {
          expect(res).to.have.status(200);
        }).catch(err => {
          console.log(err.message);
        });
      });
    });
  });
});

describe('User Log Out', function () {
  this.slow(10000);
  it('should return 200 and logs out user', async () => {
    var user = await User.findOne({ 'email': 'test@user.com' });
    await chai.request(app).get('/logout/' + user._id).then((res) => {
      expect(res).to.have.status(200);
    }).catch(err => {
      console.log(err.message);
    });
  });
});