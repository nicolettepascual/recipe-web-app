const jwt = require('jsonwebtoken');
const User = require('../model/user.model');
const UserService = require('../service/user.service');
let found_token;
module.exports = async function (req, res, next) {
  try {
    const userId = req.query.user;
    var found_user = await UserService.findUser({ '_id': userId });
    if (found_user != null) {
      found_token = found_user.tokens[0].token;
      jwt.verify(found_token, process.env.JWT_KEY, async function (err, decoded) {
        if (err) {
          if (err.name === 'TokenExpiredError') {
            const newToken = await UserService.createToken({ email: found_user.email });
            await UserService.saveToken(found_user.email, newToken);
            return newToken;
          }
          return res.status(401).send();
        }
        req.userData = decoded;
      })
    }
    next();
  } catch (err) {
    res.status(401).json({ message: 'Auth failed: ' + err });
  }
}