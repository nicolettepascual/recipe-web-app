const mongoose = require('mongoose');
const recipeSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    mealName: { type: String, required: true },
    description: { type: String, required: true },
    ingredients: { type: String, required: true },
    instructions: { type: String, required: true },
    prepTime: { type: String, required: true },
    imageURL: { type: String, required: true },
    userId: { type: String, required: true }
},
    {
        timestamps: true
    });

module.exports = mongoose.model('Recipe', recipeSchema, 'recipes');