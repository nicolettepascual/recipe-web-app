const User = require('../model/user.model');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');

exports.homePage = async function (query) {
    try {
        var user = await User.findOne(query);
        return user;
    } catch (e) {
        throw Error(e);
    }
}

//Login
exports.findUser = async function (query) {
    try {
        var found_user = await User.findOne(query);
        return found_user;
    } catch (e) {
        throw Error(e)
    }
}

exports.checkPassword = async function (sentPassword, foundPassword) {
    try {
        var isValid = await bcrypt.compare(sentPassword, foundPassword);
        return isValid;
    } catch (e) {
        throw Error(e)
    }
}

exports.createToken = async function (email) {
    try {
        const token = jwt.sign({ email: email }, process.env.JWT_KEY, { expiresIn: "1h" });
        return token;
    } catch (e) {
        throw Error(e);
    }
}

exports.saveToken = async function (email, token) {
    try {
        await User.updateOne({ "email": email }, { $set: { "tokens.0": { token: token } } })
    } catch (e) {
        throw Error(e)
    }
}

//Register
exports.encryptPassword = async function (password) {
    try {
        var hashed_password = await bcrypt.hash(password, 10);
        return hashed_password;
    } catch (e) {
        throw Error(e)
    }
}

exports.saveUser = async function (name, email, password) {
    try {
        const new_user = new User({
            _id: mongoose.Types.ObjectId(),
            name: name,
            email: email,
            password: password,
            userType: 'user',
            tokens: [{
                token: "new user"
            }]
        });
        await new_user.save();
    } catch (e) {
        throw Error(e)
    }
}

exports.resetPassword = async function (email, password) {
    try {
        await User.updateOne({ 'email': email }, { $set: { password: password } })
    } catch (e) {
        throw Error(e)
    }
}

exports.logout = async function (query) {
    try {
        await User.updateOne(query, { $unset: { "tokens.0": 1 } })
    } catch (e) {
        throw Error(e)
    }
}