const Recipe = require('../model/recipe.model');

exports.addRecipe = async function (recipeData) {
    try {
        await recipeData.save();
    } catch (e) {
        throw Error(e);
    }
}

exports.getRecipes = async function (query, userType) {
    try {
        /*if (userType === 'user') {
            var recipes = await Recipe.find(query);
        } else {*/
            var recipes = await Recipe.find({});
        //}
        return recipes;
    } catch (e) {
        throw Error(e)
    }
}

exports.getRecipe = async function (query) {
    try {
        var recipes = await Recipe.findOne(query);
        return recipes;
    } catch (e) {
        throw Error(e)
    }
}

exports.updateRecipe = async function (query, mealName, description, ingredients, instructions, prepTime, imageURL) {
    try {
        if (imageURL) {
            await Recipe.updateOne(query, {
                $set:
                {
                    mealName: mealName,
                    description: description,
                    ingredients: ingredients,
                    instructions: instructions,
                    prepTime: prepTime,
                    imageURL: imageURL
                }
            });
        } else {
            await Recipe.updateOne(query, {
                $set:
                {
                    mealName: mealName,
                    description: description,
                    ingredients: ingredients,
                    instructions: instructions,
                    prepTime: prepTime
                }
            });
        }
    } catch (e) {
        throw Error(e);
    }
}

exports.deleteRecipe = async function (query) {
    try {
        await Recipe.deleteOne(query);
    } catch (e) {
        throw Error(e);
    }
}