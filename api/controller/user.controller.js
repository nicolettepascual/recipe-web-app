const UserService = require('../service/user.service');
const RecipeService = require('../service/recipe.service');
const ObjectId = require('mongodb').ObjectID;

//Home Page
/*exports.home = async function (req, res) {
    res.redirect('/home');
}*/

exports.homePage = async function (req, res) {
    try {
        var user = await UserService.homePage({ '_id': req.query.user });
        if (user != null) {
            var recipes = await RecipeService.getRecipes({ 'userId': req.query.user }, user.userType);
            res.render('home', { user, recipes });
        } else {
            res.render('login');
        }
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
}

//Reset Password
exports.resetPasswordPage = async function (req, res, next) {
    try {
        return res.render('reset_password');
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
}

//Login
exports.login = async function (req, res, next) {
    let hasErrors = false;
    let errors = [];

    if (!req.body.email) {
        errors.push({ 'email': 'Email not received' })
        hasErrors = true;
    }
    if (!req.body.password) {
        errors.push({ 'password': 'Password not received' })
        hasErrors = true;
    }

    if (hasErrors) {
        return res.status(422).json({ message: "Invalid input", errors: errors });
    } else {
        var found_user = await UserService.findUser({ 'email': req.body.email });
        if (!found_user) {
            return res.status(401).json({ message: "Auth error: Email not found" });
        } else {
            try {
                var isValid = await UserService.checkPassword(req.body.password, found_user.password);
                if (!isValid) {
                    return res.status(401).json({ message: "Auth error: Incorrect password" })
                } else {
                    const token = await UserService.createToken(req.body.email);
                    await UserService.saveToken(req.body.email, token);
                    return res.redirect('/?user=' + found_user._id);
                }
            } catch (e) {
                return res.status(500).json({ message: e.message });
            }
        }
    }
}

//Register
exports.registerPage = async function (req, res, next) {
    res.render('register');
}

exports.register = async function (req, res, next) {
    let hasErrors = false;
    let errors = [];
    if (!req.body.name) {
        errors.push({ 'name': 'Name not received' })
        hasErrors = true;
    }
    if (!req.body.email) {
        errors.push({ 'email': 'Email not received' })
        hasErrors = true;
    }
    if (!req.body.password) {
        errors.push({ 'password': 'Password not received' })
        hasErrors = true;
    }

    if (hasErrors) {
        return res.status(401).json({ message: "Invalid input", errors: errors });
    } else {
        //encrypt user password
        try {
            var hashed_password = await UserService.encryptPassword(req.body.password);
            await UserService.saveUser(req.body.name, req.body.email, hashed_password);
            return res.redirect('/');
        } catch (e) {
            return res.status(500).json({ status: 500, message: e.message });
        }
    }
}

exports.resetPassword = async function (req, res, next) {
    try {
        var hashed_password = await UserService.encryptPassword(req.body.password);
        await UserService.resetPassword(req.body.email, hashed_password);
        return res.redirect('/');
    } catch (e) {
        return res.status(500).json({ status: 500, message: e.message });
    }
}

//Log Out
exports.logout = async function (req, res, next) {
    try {
        var userId = req.params.id;
        await UserService.logout({ '_id': ObjectId(userId) });
        return res.redirect('/');
    } catch (e) {
        return res.status(500).json({ status: 500, message: e.message });
    }
}