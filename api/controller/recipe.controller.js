const ObjectId = require('mongodb').ObjectID;
const Recipe = require('../model/recipe.model');
const RecipeService = require('../service/recipe.service');
const UserService = require('../service/user.service');
const mongoose = require('mongoose');
const JSAlert = require('js-alert');
const s3 = require('../../config/s3/config');
const s3Client = s3.s3Client;
const params = s3.uploadParams;

//Add Recipe
exports.addRecipePage = async function (req, res, next) {
    try {
        var user = await UserService.homePage({ '_id': req.query.user });
        if (user != null) {
            res.render('add_recipe', { user });
        } else {
            res.render('login');
        }
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
}

exports.addRecipe = async function (req, res, next) {
    try {
        if (req.file == undefined) { 
            const recipeData = new Recipe({
                _id: mongoose.Types.ObjectId(),
                mealName: req.body.mealName,
                description: req.body.description,
                ingredients: req.body.ingredients,
                instructions: req.body.instructions,
                prepTime: req.body.prepTime,
                imageURL: process.env.AWS_S3 + "default.jpg",
                userId: req.body.id
            });
            await RecipeService.addRecipe(recipeData);
        } else {
            params.Key = req.file.originalname;
            params.Body = req.file.buffer;

            s3Client.upload(params, async function (err) {
                if (err) {
                    res.status(500).json("Error uploading image: " + err);
                } else {
                    const recipeData = new Recipe({
                        _id: mongoose.Types.ObjectId(),
                        mealName: req.body.mealName,
                        description: req.body.description,
                        ingredients: req.body.ingredients,
                        instructions: req.body.instructions,
                        prepTime: req.body.prepTime,
                        imageURL: process.env.AWS_S3 + req.file.originalname,
                        userId: req.body.id
                    });
                    await RecipeService.addRecipe(recipeData);
                }
            });    
        }
        return res.redirect('/?user=' + req.body.id);
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
}

exports.getRecipe = async function (req, res, next) {
    try {
        var recipeId = req.params._id;
        var recipe = await RecipeService.getRecipe({ '_id': recipeId });
        var user = await UserService.homePage({ '_id': req.query.user });
        if (req.params.edit) {
            return res.render('edit_recipe', { recipe, currentUser: req.query.user, userType: user.userType });
        } else {
            return res.render('recipe', { recipe, currentUser: req.query.user, userType: user.userType });
        }
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
}

exports.updateRecipe = async function (req, res, next) {
    try {
        var recipeId = req.params._id;
        var mealName = req.body.mealName;
        var description = req.body.description;
        var ingredients = req.body.ingredients;
        var instructions = req.body.instructions;
        var prepTime = req.body.prepTime;

        if (req.file == undefined) {
            await RecipeService.updateRecipe({ '_id': ObjectId(recipeId) },
                mealName, description, ingredients, instructions, prepTime);
        } else {
            var imageURL = process.env.AWS_S3 + req.file.originalname;
            params.Key = req.file.originalname;
            params.Body = req.file.buffer;
            s3Client.upload(params, async function (err) {
                if (err) {
                    res.status(500).json("Error uploading image: " + err);
                } else {
                    await RecipeService.updateRecipe({ '_id': ObjectId(recipeId) },
                        mealName, description, ingredients, instructions, prepTime, imageURL);
                }
            });
        }
        this.setTimeout(() => {
            return res.redirect('/?user=' + req.params.user);
        }, 3000);
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
}

exports.deleteRecipe = async function (req, res, next) {
    try {
        var recipeId = req.params._id;
        var userId = req.params.user;
        await RecipeService.deleteRecipe({ '_id': ObjectId(recipeId) });
        return res.redirect('/?user=' + userId);
    } catch (e) {
        return res.status(500).json({ status: 500, message: e.message });
    }
}