const express = require('express');
const router = express.Router();
const checkAuth = require('./middleware/check-auth');
const methodOverride = require('method-override');
const UserController = require('../api/controller/user.controller');
const RecipeController = require('../api/controller/recipe.controller');
const upload = require ('../config/multer/config');

router.use(methodOverride('_method'));

router.post('/register', UserController.register);

router.get('/register', UserController.registerPage);

router.post('/login', UserController.login);

//router.get('/home', UserController.homePage);

router.get('/resetpassword', UserController.resetPasswordPage);

router.put('/resetpassword', UserController.resetPassword);

router.get('/logout/:id', UserController.logout);


//Recipes -------
router.get("/add/recipe", checkAuth, RecipeController.addRecipePage);
router.post('/add/recipe', upload.single("myImage"), RecipeController.addRecipe);

//displaying one recipe
router.get('/recipe/:_id/', checkAuth, RecipeController.getRecipe);

//editing a recipe
router.get('/recipe/:edit/:_id', checkAuth, RecipeController.getRecipe);
router.put('/edit/:_id/:user', upload.single("myImage"), RecipeController.updateRecipe);

//delete
router.get('/delete/:_id/:user', RecipeController.deleteRecipe);

//router.post('/upload', upload.single("myImage"), RecipeController.doUpload);

module.exports = router;