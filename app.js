const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const path = require('path');
const checkAuth = require('./api/middleware/check-auth');
const UserController = require('./api/controller/user.controller');

require('dotenv').config();

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

//EJS
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

const routes = require('./api/routes');
app.use(routes);

const mongoose = require('mongoose');
let dbConn = "mongodb+srv://admin:admin@cluster0-awnkb.mongodb.net/task?retryWrites=true&w=majority";
//connect to the database
mongoose.connect(dbConn, { useNewUrlParser: true, useUnifiedTopology: true }).then(() => {
  console.log('Connected to the database');
}).catch(err => {
  console.log('Error connecting to the database: ' + err);
  process.exit();
});

app.get('/', checkAuth, UserController.homePage);

//Listen
app.listen(8000, () => {
  console.log('App listening to Port 8000');
})

module.exports = app;